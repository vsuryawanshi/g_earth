import Cesium from 'cesium/Cesium';
import 'cesium/Widgets/widgets.css';
import './css/main.css';

Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0OTRlNWI4My04NzAxLTQyMTktODMxOS02NTEwN2IzMGU4N2YiLCJpZCI6NjA2LCJpYXQiOjE1MjUzMzE2MjZ9.XJb_PZaHLDZw2anOFdzTFhTAebGbDZwiLKTo69i-Y2A';

var viewer = new Cesium.Viewer('cesiumContainer', {
    imageryProvider : new Cesium.ArcGisMapServerImageryProvider({
        url : '//services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer'
    }),
    baseLayerPicker : false
});

var layers = viewer.scene.imageryLayers;
var blackMarble = layers.addImageryProvider(new Cesium.createTileMapServiceImageryProvider({
    url : '//cesiumjs.org/tilesets/imagery/blackmarble',
    maximumLevel : 8,
    credit : 'Black Marble imagery courtesy NASA Earth Observatory'
}));